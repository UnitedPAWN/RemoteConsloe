#pragma once


#include "connection.h"
#include <boost/asio.hpp>
#include "rcon.hpp"
#include "crypto.hpp"

class RCONConnection : public Connection {
private:
	boost::asio::io_service io_service;
	boost::asio::io_service::work work;

	boost::asio::ip::udp::endpoint endpoint;
	boost::asio::ip::udp::resolver resolver;
	boost::asio::ip::udp::resolver::query resolver_query;

	std::unique_ptr<rcon> RCON;
	std::shared_ptr<CryptoSuit> crypto;
	std::string id_token;
	std::string name;
	std::string password;
	std::string rcon_password;

	bool logged_in = false;

	void socket_handler(const std::string &output, const boost::system::error_code &ec);
public:
	RCONConnection(const std::string & host, const std::string & port, const std::string & rcon_password, const std::string & name, const std::string & password);

	virtual bool sendCommand(const std::string & command) override;
};




















