

#include "crypto.hpp"
#include <cstring>








const char * personalization = "x8J$AB}m9@wts>oRo 5.M;JO2}c%Q-:!";


//===================================================| Exception |=============================================|
 CryptoException::CryptoException(const char * text, int err) {
	 number = err;
	 size_t len = strlen(text);
	 this->text = new char[len+1];
	 strcpy(this->text, text);
}

 CryptoException::CryptoException(const CryptoException & o) {
	 number = o.number;
	 size_t len = strlen(o.text);
	 this->text = new char[len + 1];
	 strcpy(this->text, o.text);
 }

 CryptoException::CryptoException(CryptoException && o) {
	 number = o.number;
	 this->text = o.text;
	 o.text = nullptr;
 }

 CryptoException::~CryptoException() noexcept {
	 if (text) delete[] text;
 }

 CryptoException & CryptoException::operator=(const CryptoException & o) {
	 number = o.number;
	 size_t len = strlen(o.text);
	 this->text = new char[len + 1];
	 strcpy(this->text, o.text);
	 return *this;
 }

 CryptoException & CryptoException::operator=(CryptoException && o) {
	 number = o.number;
	 this->text = o.text;
	 o.text = nullptr;
	 return *this;
 }
//===================================================| Data |=============================================|

//===================================================| Entropy |=============================================|
Entropy entropy;

Entropy::Entropy() {
	mbedtls_entropy_init(&entropy);
	mbedtls_entropy_gather(&entropy);
}

Entropy::~Entropy() {
	mbedtls_entropy_free(&entropy);
}
//===================================================| HASH |=============================================|
crypto_data HASH::hash(crypto_data data, mbedtls_md_type_t type) {
	unsigned char buffer[129];
	const mbedtls_md_info_t * t_type = mbedtls_md_info_from_type(type);
	unsigned char len = mbedtls_md_get_size(t_type);
	int err;
	if (err = mbedtls_md(t_type, data.data(), data.length(), buffer)) throw CryptoException("Can't hash.", err);
	return crypto_data(buffer, len);
}
//===================================================| HMAC |=============================================|
HMAC::HMAC(mbedtls_md_type_t type) : type(type) {

}

HMAC::~HMAC() {

}

void HMAC::hash(const char * str, const char * pass, char dest[90], mbedtls_md_type_t type) {
	unsigned char buffer[65];
	size_t olen;
	mbedtls_md_hmac(mbedtls_md_info_from_type(type), reinterpret_cast<const unsigned char *>(pass), strlen(pass), reinterpret_cast<const unsigned char *>(str), strlen(str), buffer);
	mbedtls_base64_encode(reinterpret_cast<unsigned char *>(dest), 89, &olen, buffer, 64);
	dest[88] = 0;
}

crypto_data HMAC::hash(crypto_data str, crypto_data pass, mbedtls_md_type_t type) {
	unsigned char buffer[65];
	mbedtls_md_hmac(mbedtls_md_info_from_type(type), pass, pass.length(), str, str.length(), buffer);
	return crypto_data(buffer, 64);
}

void HMAC::setKey(const char * key) {
	if (strlen(key) != 32) throw CryptoException("HMAC: Key does not have 32 cahracters.", 0);
	key = key;
}

void HMAC::setKey(const crypto_data & key) {
	if (key.length() != 32) throw CryptoException("HMAC: Key does not have 32 cahracters.", 0);
	this->key = key;
}

crypto_data HMAC::hash(const crypto_data & str, mbedtls_md_type_t type) {
	unsigned char buffer[65];
	mbedtls_md_hmac(mbedtls_md_info_from_type(type), key, key.length(), str, str.length(), buffer);
	return crypto_data(buffer, 64);
}
//===================================================| AES |=============================================|
AES::AES() {
	mbedtls_aes_init(&ectx);
	mbedtls_aes_init(&dctx);
}

AES::AES(const crypto_data & key) {
	mbedtls_aes_init(&ectx);
	mbedtls_aes_init(&dctx); 
	setKey(key);
}

AES::~AES() {
	mbedtls_aes_free(&ectx);
	mbedtls_aes_free(&dctx);
}

crypto_data AES::encrypt(crypto_data iv, const crypto_data & data, const crypto_data & key, bool base64) {
	if (iv.length() != 16) throw CryptoException("AES ENC: IV is not length 16.");
	if (key.length() != 32) throw CryptoException("AES ENC: Key is not length 32.");
	int err;

	crypto_data padded = crypto_data::addPadding(data);
	crypto_data output(padded.length());
	
	mbedtls_aes_context ectx;
	mbedtls_aes_init(&ectx);
	mbedtls_aes_setkey_enc(&ectx, key, 256);
	err = mbedtls_aes_crypt_cbc(&ectx, MBEDTLS_AES_ENCRYPT, padded.length(), iv.data(), padded.data(), output.data());
	mbedtls_aes_free(&ectx);
	
	if(err) throw CryptoException("AES: Can't encrypt.", err);

	if (base64) {
		crypto_data base64d = output.base64();
		return base64d;
	}
	else return output;
}

crypto_data AES::decrypt(crypto_data iv, const crypto_data & data, const crypto_data & key, bool base64) {
	if (iv.length() != 16) throw CryptoException("AES DEC: IV is not length 16.");
	if (key.length() != 32) throw CryptoException("AES DEC: Key is not length 32.");
	int err;

	crypto_data padded = crypto_data::addPadding(data);
	crypto_data output(padded.length());

	mbedtls_aes_context ectx;
	mbedtls_aes_init(&ectx);
	mbedtls_aes_setkey_dec(&ectx, key, 256);
	err = mbedtls_aes_crypt_cbc(&ectx, MBEDTLS_AES_DECRYPT, padded.length(), iv.data(), padded.data(), output.data());
	mbedtls_aes_free(&ectx);

	if (err) throw CryptoException("AES: Can't decrypt.", err);

	if (base64) {
		crypto_data base64d = output.base64();
		return base64d;
	}
	else return output;
}

void AES::setKey(const char * key) {
	if (strlen(key) != 32) throw CryptoException("AES: Key does not have 32 cahracters.", 0);
	mbedtls_aes_setkey_enc(&ectx, reinterpret_cast<const unsigned char *>(key), 256);
	mbedtls_aes_setkey_dec(&dctx, reinterpret_cast<const unsigned char *>(key), 256);
}

void AES::setKey(const crypto_data & key) {
	if (key.length() != 32) throw CryptoException("AES: Key does not have 32 cahracters.", 0);
	
	int err;
	if (err = mbedtls_aes_setkey_enc(&ectx, key, 256)) throw CryptoException("AES: Can't set encryption key.", err);
	if (err = mbedtls_aes_setkey_dec(&dctx, key, 256)) throw CryptoException("AES: Can't set decryption key.", err);
}

crypto_data AES::encrypt(crypto_data iv, const crypto_data & data, bool base64) {
	if (iv.length() != 16) throw CryptoException("AES ENC: IV is not length 16.");
	int err;

	crypto_data padded = crypto_data::addPadding(data);
	crypto_data output(padded.length());
	if(err = mbedtls_aes_crypt_cbc(&ectx, MBEDTLS_AES_ENCRYPT, padded.length(), iv.data(), padded.data(), output.data())) throw CryptoException("AES: Can't encrypt.", err);

	if (base64) {
		crypto_data base64d = output.base64();
		return base64d;
	}
	else return output;
}

crypto_data AES::decrypt(crypto_data iv, const crypto_data & data, bool base64) {
	if (iv.length() != 16) throw CryptoException("AES ENC: IV is not length 16.");
	
	if (base64) {
		crypto_data decoded(crypto_data::fromBase64(data));

		crypto_data result(decoded.length());
		int err;
		if(err = mbedtls_aes_crypt_cbc(&dctx, MBEDTLS_AES_DECRYPT, decoded.length(), iv, decoded, result)) throw CryptoException("AES: Can't decrypt.", err);
		return crypto_data::removePadding(result);
	}
	else {
		crypto_data result(data.length());
		int err;
		if (err = mbedtls_aes_crypt_cbc(&dctx, MBEDTLS_AES_DECRYPT, data.length(), iv, data, result)) throw CryptoException("AES: Can't decrypt.", err);
		return crypto_data::removePadding(result);
	}
}
//===================================================| Random |=============================================|
Random::Random() {
	mbedtls_ctr_drbg_init(&ctx);
	mbedtls_ctr_drbg_seed(&ctx, mbedtls_entropy_func, entropy.get(), reinterpret_cast<const unsigned char *>(personalization), 32);
}

Random::~Random() {
	mbedtls_ctr_drbg_free(&ctx);
}

void Random::generate(size_t len, unsigned char * output) {
	mbedtls_ctr_drbg_random(&ctx, output, len);
}

crypto_data Random::generate(size_t len) {
	crypto_data data(len);
	mbedtls_ctr_drbg_random(&ctx, data.data(), len);
	return data;
}






























Random CryptoSuit::random;

void CryptoSuit::loadKeys(const crypto_data & keys) {
	if (keys[0] == '1' && keys[1] == '#') {
		if(keys.length() != 66) throw CryptoException("loadKeys: invalid length.", 0);
		
		aes.setKey(crypto_data(keys, 32, 2));
		hmac.setKey(crypto_data(keys, 32, 34));
	}
	else throw CryptoException("loadKeys: invalid format.", 0);
}

crypto_data CryptoSuit::generateKeys() { 
	crypto_data aes_key = random.generate(32);
	crypto_data hmac_key = random.generate(32);
	aes.setKey(aes_key);
	hmac.setKey(hmac_key);

	crypto_data keys(66);
	keys[0] = '1';
	keys[1] = '#';
	keys.fill(aes_key, 32, 2);
	keys.fill(hmac_key, 32, 34);
	return keys;
}
