
#pragma once

#include <cstring>
#include <functional>

class amx_string {
protected:
	char *str;
	int len;
	bool unknownLen = false;
public:
	amx_string() : str(nullptr), len(0), unknownLen(false) {};

	amx_string(const amx_string & other);
	amx_string(amx_string && other);
	virtual ~amx_string();
//----------------------------------------------------------------------|
	operator const char *() const { return str; };
	virtual amx_string &operator=(const amx_string &str);
	virtual amx_string &operator=(amx_string &&str);
	virtual amx_string &operator=(const char *str);
	bool operator==(const amx_string &str) const;

	virtual amx_string &operator+=(const amx_string & str);
	virtual amx_string &operator+=(const char * str);

	virtual char & operator[](size_t pos) { return str[pos]; };
	virtual char operator[](size_t pos) const { return str[pos]; };
	virtual char & operator[](int pos) { return str[pos]; };
	virtual char operator[](int pos) const { return str[pos]; };

	const char *c_str() const { return str; };
	int length() {
		if (unknownLen) len = strlen(str);
		return len;
	};
	int length() const { return unknownLen ? strlen(str) : len; };

	virtual void resize(size_t size);
	virtual char * data() { 
		unknownLen = true;
		return str;
	};
	const char * data() const { return str; };
	const char * cdata() const { return str; };
};



//===========================================================| Implementation |=============================================================================|

inline amx_string::amx_string(const amx_string & other) {
	this->unknownLen = other.unknownLen;
	this->len = other.length();
	this->str = new char[len + 1];
	strcpy(this->str, other.str);
}

inline amx_string::amx_string(amx_string && other) {
	this->unknownLen = other.unknownLen;
	this->len = other.len;
	this->str = other.str;
	other.str = nullptr;
}

inline amx_string::~amx_string() {
	if (this->str) delete[] this->str;
}

inline amx_string &amx_string::operator=(const amx_string &str) {
	if (this->str) delete[] this->str;
	this->str = new char[str.length() + 1];
	this->len = str.length();
	strcpy(this->str, str.str);
	this->unknownLen = str.unknownLen;
	return *this;
}

inline amx_string &amx_string::operator=(amx_string && str) {
	if (this->str) delete[] this->str;
	this->str = str.str;
	str.str = nullptr;
	this->len = str.len;
	this->unknownLen = str.unknownLen;
	return *this;
}

inline amx_string &amx_string::operator=(const char *str) {
	if (this->str) delete[] this->str;
	if (!str) {
		this->str = nullptr;
		this->len = 0;
	}
	else {
		size_t len = strlen(str);
		this->str = new char[len + 1];
		this->len = len;
		strcpy(this->str, str);
	}
	this->unknownLen = false;
	return *this;
}

inline bool amx_string::operator==(const amx_string &str) const {
	if (this->str == nullptr) return str.str == nullptr;
	if (str.str == nullptr) return false;
	if (this->length() != str.length()) return false;
	return strcmp(this->str, str.str) == 0;
}

inline amx_string & amx_string::operator+=(const amx_string & str) {
	if (this->str == nullptr) return this->operator=(str);
	if (unknownLen) this->len = strlen(this->str);
	this->len += str.length();
	char * tmp = this->str;
	this->str = new char[this->len + 1];
	strcpy(this->str, tmp);
	strcat(this->str, str.str);
	delete[] tmp;
	return *this;
}

inline amx_string & amx_string::operator+=(const char * str) {
	if (this->str == nullptr) return this->operator=(str);
	if (unknownLen) this->len = strlen(this->str);
	this->len += strlen(str);
	char * tmp = this->str;
	this->str = new char[this->len + 1];
	strcpy(this->str, tmp);
	strcat(this->str, str);
	delete[] tmp;
	return *this;
}

inline void amx_string::resize(size_t len) {
	if (this->str) {
		char * tmp = this->str;
		this->str = new char[len];
		strncpy(this->str, tmp, len);
		this->str[len - 1] = '\0';
		delete[] tmp;
	}
	else this->str = new char[len];
}
