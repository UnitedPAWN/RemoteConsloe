







static const int Version = 2;

#include <thread>

#include "rcon_con.h"
#include "rcon.hpp"

static std::string DecryptVerifyMessage(const std::string output, const std::string & id_token, CryptoSuit & crypto) {
	size_t delim[4];
	delim[0] = output.find(';');
	delim[1] = output.find(';', delim[0] + 1);
	delim[2] = output.find('|', delim[1] + 1);
	delim[3] = output.find('|', delim[2] + 1);
	if (delim[0] == output.npos || delim[1] == output.npos || delim[2] == output.npos || delim[3] == output.npos) {
		throw std::runtime_error("Unknown response. Server does not support unit remote console or supports different version.");
	}

	if (id_token.compare(output.substr(0, delim[1] + 1)) != 0) {
		throw std::runtime_error("Different connection attempred. Terminateing.");
	}

	crypto_data hmac = crypto.getHMAC().hash(crypto_data(output.substr(0, delim[3]).c_str()));
	if (hmac != crypto_data::fromBase64(output.substr(delim[3] + 1).c_str())) {
		throw std::runtime_error("Incorrect HMAC. Possible attempt to hack.");
	}

	crypto_data data = crypto.getAES().decrypt(crypto_data::fromBase64(output.substr(delim[1] + 1, delim[2] - delim[1] - 1).c_str()), crypto_data(output.substr(delim[2] + 1, delim[3] - delim[2] - 1).c_str()), true);
	return std::string(reinterpret_cast<char *>(data.data()), data.length());
}

void RCONConnection::socket_handler(const std::string &output, const boost::system::error_code &ec) {
	try {
		if (!ec) {
			if (id_token.length() < 2) {
				if (output.compare("Invalid RCON password.") == 0) {
					throw std::runtime_error("Wrong RCON password. Can't connect to server.");
				}
				size_t delim[2];
				delim[0] = output.find(';');
				delim[1] = output.find(';', delim[0] + 1);
				if (delim[0] == output.npos || delim[1] == output.npos) {
					throw std::runtime_error("Unknown response. Server does not support unit remote console or supports different version. Response:\n    " + output);
				}
				id_token += output.substr(0, delim[1]) + ";";

				crypto_data hmac = HMAC::hash(crypto_data(output.substr(delim[1] + 1).c_str()), crypto_data(password.c_str()));
				
				crypto = std::make_shared<CryptoSuit>(HASH::hash(crypto_data(("AES_KEY-" + password + "-" + id_token + "-AES_KEY").c_str()), MBEDTLS_MD_SHA256), HASH::hash(crypto_data(("HMAC_DATA-" + password + "-" + id_token + "-HMAC_DATA").c_str()), MBEDTLS_MD_SHA256));
				std::string cmd = id_token + "1|" + name + "|" + hmac.base64().str();
				RCON->send_command(rcon_password, cmd);
			}
			else {
				if (logged_in) {
					announceReply(DecryptVerifyMessage(output, id_token, *crypto));
				}
				else if (output.compare("LOGIN::FAIL") == 0) std::cout << "Failed to log in. Can't connect to the server." << std::endl;
				else {
					std::string resp = DecryptVerifyMessage(output, id_token, *crypto);

					if (resp.compare("LOGIN::SUCCESS") == 0) {
						logged_in = true;
						std::cout << "\nSuccesfully logged in." << std::endl;
					}
					else throw std::runtime_error("Unknown response. Server does not support unit remote console or supports different version. Response:\n    " + resp);
				}
			}
		}
		else if (ec != boost::asio::error::operation_aborted) {
			throw boost::system::system_error(ec);
		}
	}
	catch (const std::exception & e) {
		announceException(e);
	}
}

void io_service_runner(boost::asio::io_service * io_service) {
	io_service->run();
}

RCONConnection::RCONConnection(const std::string & host, const std::string & port, const std::string & rcon_password, const std::string & name, const std::string & password) : resolver(io_service), resolver_query(host, port), work(io_service), rcon_password(rcon_password), name(name), password(password) {	
	for (auto it = resolver.resolve(resolver_query);
		it != boost::asio::ip::udp::resolver::iterator(); it++) {
		if (it->endpoint().address().is_v4()) {
			endpoint = *it;
			break;
		}
	}

	RCON.reset(new rcon(io_service, endpoint));
	RCON->set_timeout(boost::posix_time::milliseconds(300));

	RCON->set_receive_handler(std::bind(&RCONConnection::socket_handler, this, std::placeholders::_1, std::placeholders::_2));

	std::thread worker_thread(io_service_runner, &io_service);
	worker_thread.detach();

	RCON->send_command(rcon_password, std::string("INIT::CONNECTION ") + std::to_string(Version));
}

bool RCONConnection::sendCommand(const std::string & command) {
	if (!crypto) return false;
	crypto_data iv = CryptoSuit::getRandom().generate(16);
	crypto_data cipher = crypto->getAES().encrypt(iv, crypto_data(command.c_str()));
	std::string cmd = id_token + iv.base64().str() + "|" + cipher.str();
	RCON->send_command(rcon_password, cmd + "|" + crypto->getHMAC().hash(crypto_data(cmd.c_str())).base64().str());
	return true;
}
