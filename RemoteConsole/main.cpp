

#include <iostream>
#include <stdexcept>
#include <string>

#include "rcon_con.h"
#include "crypto.hpp"

void read_command(std::string &command) {
	command.clear();
	do {
		std::getline(std::cin, command).good();
	} while (command.length() < 1);
}

void print_error(const char *message) {
	std::cerr << "Error: " << message << std::endl;
}

void handle_exception(const std::exception & e) {
	print_error(e.what());
	std::cout << "Press enter to continue..." << std::endl;
	std::cin.clear();
	std::cin.get();
	std::terminate();
}

void handle_reply(const std::string & reply) {
	std::cout << reply << std::endl;
}

int main(int argc, char ** argv) {
	std::string host;
	std::string port;
	std::string password;
	std::string command;

	bool logged_in(false);

	if (argc >= 4) {
		host = argv[1];
		port = argv[2];
		password = argv[3];
	}
	else {
		std::cout << "Zadaj IP serveru:" << std::endl;
		read_command(host);
		std::cout << "Zadaj Port serveru:" << std::endl;
		read_command(port);
		std::cout << "Zadaj RCON heslo:" << std::endl;
		read_command(password);
	}

	try {
		std::string name, pass;
		std::cout << "Zadaj svoj nick:" << std::endl;
		read_command(name);
		std::cout << "Zadaj svoje heslo:" << std::endl;
		read_command(pass);

		std::shared_ptr<Connection> connection = std::make_shared<RCONConnection>(host, port, password, name, pass);

		connection->sedReplyHandler(handle_reply);
		connection->sedExceptionHandler(handle_exception);

		do {
			try {
				read_command(command);
				connection->sendCommand(command);
			}
			catch (boost::system::system_error &e) {
				print_error(e.what());
			}
		} while (1);
	}
	catch (CryptoException &e) {
		print_error(e.what());
		std::cout << "Press enter to continue..." << std::endl;
		std::cin.clear();
		std::cin.get();
		return 1;
	}
	catch (std::exception &e) {
		print_error(e.what());
		std::cout << "Press enter to continue..." << std::endl;
		std::cin.clear();
		std::cin.get();
		return 1;
	}
	return 0;
}
