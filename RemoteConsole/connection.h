#pragma once

#include <exception>
#include <functional>
#include <string>

class Connection {
private:
	std::function<void(const std::string &)> reply_handler;
	std::function<void(const std::exception &)> exception_handler;
protected:
	void announceReply(const std::string & reply) { if(reply_handler) reply_handler(reply); };
	void announceException(const std::exception & exception) { if(exception_handler) exception_handler(exception); };
public:
	virtual ~Connection() {};

	virtual bool sendCommand(const std::string & command) = 0;
	virtual void sedReplyHandler(const std::function<void(const std::string &)> & handler) { reply_handler = handler; };
	virtual void sedExceptionHandler(const std::function<void(const std::exception &)> & handler) { exception_handler = handler; };
};







