#pragma once

#include <exception>
#include <string>
#include <iostream>

#include "mbedtls/base64.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/hmac_drbg.h"
#include "mbedtls/aes.h"
#include "mbedtls/error.h"

inline void printCryptoErr(int err) {
	if (err) {
		char buff[256];
		buff[0] = 0;
		mbedtls_strerror(err, buff, 255);
		std::cout << buff << std::endl;
	}
}

//---------------------------------------------------------------------------------------------------------------------------------------|
class CryptoException : std::exception {
	char * text = nullptr;
	int number;
public:
	CryptoException(const char * text, int err = 0);
	CryptoException(const CryptoException & o);
	CryptoException(CryptoException && o);
	~CryptoException() noexcept;

	CryptoException & operator=(const CryptoException & o);
	CryptoException & operator=(CryptoException && o);

	const char * what() const noexcept override { return text; };
	int error() const { return number; };
};
//---------------------------------------------------------------------------------------------------------------------------------------|
class crypto_data {
public:
	static unsigned char * toUChar(char * data) { return reinterpret_cast<unsigned char *>(data); };
	static const unsigned char * toUChar(const char * data) { return reinterpret_cast<const unsigned char *>(data); };

	static crypto_data ref(char * data) { return ref(toUChar(data), strlen(data)); };
	static crypto_data ref(unsigned char * data, size_t len) {
		crypto_data cdata;
		cdata.len = len;
		cdata.mData = data;
		cdata.own = false;
		return cdata;
	};
	static const crypto_data ref(const char * data) { return ref(toUChar(data), strlen(data)); };
	static const crypto_data ref(const unsigned char * data, size_t len) {
		crypto_data cdata;
		cdata.len = len;
		cdata.mData = const_cast<unsigned char *>(data);
		cdata.own = false;
		return cdata;
	};
	static crypto_data fromBase64(const char * base64Data) { return fromBase64(base64Data, strlen(base64Data)); };
	static crypto_data fromBase64(const crypto_data & base64Data) { return fromBase64(base64Data.data(), base64Data.length()); };
	static crypto_data fromBase64(const char * base64Data, size_t len) { return fromBase64(reinterpret_cast<const unsigned char *>(base64Data), len); };
	static crypto_data fromBase64(const unsigned char * base64Data, size_t len) {
		size_t olen;
		crypto_data data(len);
		mbedtls_base64_decode(data.data(), data.length(), &olen, base64Data, len);
		data.setLen(olen);
		return data;
	};
	static crypto_data addPadding(const crypto_data & data) {
		crypto_data padded((data.length() / 16) * 16 + 16);
		size_t len1 = data.length(), len2 = padded.length();
		unsigned char diff = static_cast<unsigned char>(len2 - len1);
		memcpy(padded.data(), data.data(), len1 * sizeof(unsigned char));
		for (size_t i = len1; i < len2; ++i) padded[i] = diff;
		return padded;
	}
	static crypto_data removePadding(const crypto_data & data, bool check = true) {
		crypto_data unpadded(data);
		size_t diff = data[data.length()-1];
		unpadded.setLen(data.length() - diff);
		size_t len = unpadded.length();

		if (check) {
			for (size_t i = data.length() - 1; i >= len; --i) if (data[i] != static_cast<unsigned char>(diff)) throw CryptoException("removePadding: invalid padding.", 0);
		}
		return unpadded;
	}

private:
	size_t len;
	unsigned char * mData;
	bool own = true;
public:
	crypto_data() : len(0), mData(nullptr) { };
	explicit crypto_data(size_t len) {
		this->len = len;
		this->mData = new unsigned char[len];
	};
	explicit crypto_data(const char * data) : crypto_data(data, strlen(data)) { };
	explicit crypto_data(const char * data, size_t len) {
		this->len = len;
		this->mData = new unsigned char[len];
		memcpy(this->mData, data, len * sizeof(char));
	};
	explicit crypto_data(const unsigned char * data, size_t len) {
		this->len = len;
		this->mData = new unsigned char[len];
		memcpy(this->mData, data, len * sizeof(unsigned char));
	}; 
	crypto_data(const crypto_data & o) {
		this->len = o.len;
		this->mData = new unsigned char[len];
		memcpy(this->mData, o.mData, this->len * sizeof(unsigned char));
	};
	crypto_data(const crypto_data & o, size_t len, size_t start = 0) {
		if (len + start > o.length()) throw CryptoException("crypto_data: Length longer than data.");
		this->len = len;
		this->mData = new unsigned char[len];
		memcpy(this->mData, o.data() + start, len * sizeof(unsigned char));
	};
	crypto_data(crypto_data && o) {
		this->len = o.len;
		this->mData = o.mData;
		o.len = 0;
		o.mData = nullptr;
		o.own = false;
	};

	crypto_data & operator=(const crypto_data & o) {
		if (this->mData && own) delete[] this->mData;
		this->len = o.len;
		this->mData = new unsigned char[len];
		memcpy(this->mData, o.mData, this->len * sizeof(char));
		this->own = true;
		return *this;
	};
	crypto_data & operator=(crypto_data && o) {
		if (this->mData && own) delete[] this->mData;
		this->len = o.len;
		this->mData = o.mData;
		o.len = 0;
		o.mData = nullptr;
		this->own = o.own;
		return *this;
	};

	~crypto_data() {
		if (this->mData && own) delete[] this->mData;
	};

	operator unsigned char *() { return mData; };
	operator const unsigned char *() const { return mData; };

	size_t length() const { return len; };

	unsigned char * data() { return mData; };
	const unsigned char * data() const { return mData; };

	std::string str() const { return std::string(reinterpret_cast<char *>(mData), this->len); };

	void setLen(size_t len) { this->len = len; };

	crypto_data base64() {
		crypto_data ret(4 * (len / 3) + 5);
		size_t olen, len = this->length();
		int err;
		if(err = mbedtls_base64_encode(ret.data(), 4 * (len / 3) + 5, &olen, mData, len)) throw CryptoException("Base64: Failed to encode.", err);
		ret.setLen(olen);
		return ret;
	};

	void fill(const crypto_data source, size_t len, size_t start = 0) {
		if(this->len - start < len) throw CryptoException("crypto_data: fill bigger than max size.", 0);
		memcpy(mData+start, source.data(), sizeof(unsigned char)*len);
	};

	bool operator==(const crypto_data & o) {
		if (o.length() != length()) return false;
		for (size_t i = 0, c_len = length(); i < c_len; ++i) {
			if (mData[i] != o.mData[i]) return false;
		}
		return true;
	}
	bool operator!=(const crypto_data & o) {
		return !(operator==(o));
	}
};
//---------------------------------------------------------------------------------------------------------------------------------------|
class Entropy {
	mbedtls_entropy_context entropy;
public:
	Entropy();
	~Entropy();

	mbedtls_entropy_context * get() { return &entropy; };
	void gather() { mbedtls_entropy_gather(&entropy); };

	operator mbedtls_entropy_context *() { return &entropy; };
};

extern Entropy entropy;
//---------------------------------------------------------------------------------------------------------------------------------------|
class HASH {
public:
	static crypto_data hash(crypto_data data, mbedtls_md_type_t type = MBEDTLS_MD_SHA512);
};
//---------------------------------------------------------------------------------------------------------------------------------------|
class HMAC {
	mbedtls_md_type_t type;
	crypto_data key;
public:
	static void hash(const char * str, const char * pass, char dest[89], mbedtls_md_type_t type = MBEDTLS_MD_SHA512);
	static crypto_data hash(crypto_data str, crypto_data pass, mbedtls_md_type_t type = MBEDTLS_MD_SHA512);

	HMAC(mbedtls_md_type_t type = MBEDTLS_MD_SHA512);
	~HMAC();

	void setKey(const char * key);
	void setKey(const crypto_data & key);

	crypto_data hash(const crypto_data & str, mbedtls_md_type_t type = MBEDTLS_MD_SHA512);
};
//---------------------------------------------------------------------------------------------------------------------------------------|
class AES {
public:
	static crypto_data encrypt(crypto_data iv, const crypto_data & data, const crypto_data & key, bool base64 = true);
	static crypto_data decrypt(crypto_data iv, const crypto_data & data, const crypto_data & key, bool base64 = true);
private:
	mbedtls_aes_context ectx;
	mbedtls_aes_context dctx;
public:
	AES();
	AES(const crypto_data & key);
	~AES();

	void setKey(const char * key);
	void setKey(const crypto_data & key);
	
	crypto_data encrypt(crypto_data iv, const crypto_data & data, bool base64 = true);
	crypto_data decrypt(crypto_data iv, const crypto_data & data, bool base64 = true);
};
//---------------------------------------------------------------------------------------------------------------------------------------|
class Random {
	mbedtls_ctr_drbg_context ctx;
public:
	Random();
	~Random();

	void generate(size_t len, unsigned char * output);
	crypto_data generate(size_t len);
};






























class CryptoSuit {
private:
	static Random random;
public:
	static Random & getRandom() { return random; };
private:
	AES aes;
	HMAC hmac;
public:
	CryptoSuit() {};
	CryptoSuit(crypto_data aes_key, crypto_data hmac_key) {
		aes.setKey(aes_key);
		hmac.setKey(hmac_key);
	};

	void loadKeys(const crypto_data & keys);
	crypto_data generateKeys();

	AES & getAES() { return aes; };
	HMAC & getHMAC() { return hmac; };
};